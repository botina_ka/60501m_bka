<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($booking)) : ?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                            <img height="150" src="https://image.flaticon.com/icons/svg/1347/1347817.svg" class="card-img" alt="<?= esc($booking['ID']); ?>">
                    </div>
                    <div class="col-md-8">
                    <div class="card-body">
<h5 class="card-title">Запись номер:</h5> <p><?= esc($booking['ID']); ?></p>
<h5 class="card-title">Номер комнаты:</h5> <p><?= esc($booking['Room_ID']); ?></p>
                        <h5 class="card-title">Забронировано с: </h5>
                        <p class="card-title"><?= esc($booking['Date_begin']); ?></p>
                        <h5 class="card-title">По: </h5>
                        <p class="card-title"><?= esc($booking['Date_end']); ?></p>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
    <?php else : ?>
        <p>Бронирование не найдено</p>
    <?php endif ?>
</div>
