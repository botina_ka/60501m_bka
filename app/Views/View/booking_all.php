<div class="container main">
<h2>Все бронирования</h2>

<?php if (!empty($booking) && is_array($booking)) : ?>

    <?php foreach ($booking as $item): ?>

        <div class="card mb-3" style="max-width: 540px;">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                        <img height="150" src="https://www.creativefabrica.com/wp-content/uploads/2019/02/Sorry-Im-Booked.jpg" class="card-img" alt="<?= esc($item['ID']); ?>">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">Забронировано с: </h5>
                        <p class="card-title"><?= esc($item['Date_begin']); ?></p>
                        <h5 class="card-title">По: </h5>
                        <p class="card-title"><?= esc($item['Date_end']); ?></p>
                        <a href="<?= base_url()?>/index.php/booking_all/view/<?= esc($item['ID']); ?>" class="btn btn-primary">Просмотреть</a>
                    </div>
                </div>
            </div>
        </div>

    <?php endforeach; ?>

<?php else : ?>

    <p>Невозможно найти бронирования</p>

<?php endif ?>
</div>
