<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($booking)) : ?>
        <div class="card mb-3" style="max-width: 540px;">
            <div class="row">
                        <div class="d-flex justify-content-between">
                            <div class="my-0">Комната:</div>
                            <div class="text-muted"><?= esc($booking['Room_ID']); ?></div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="my-0">Номер зарезервирован:</div>
                            <div class="text-muted">C:<?= esc(Time::parse($booking['Date_begin'])->toDateString() ); ?></div>
                            <div class="text-muted">По: <?= esc(Time::parse($booking['Date_end'])->toDateString() ); ?></div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="my-0">Текущий рейтинг:</div>
                            <span class="badge badge-info">199</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php else : ?>
        <p>Бронирование не найдено</p>
    <?php endif ?>
</div>
