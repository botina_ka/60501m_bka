<?php namespace App\Controllers;

use App\Models\BookingModel;
use CodeIgniter\Controller;

class Booking extends Controller
{
    public function index() //Обображение всех записей
    {
        $model = new BookingModel();
        $data ['booking'] = $model->getBooking();
        echo view('templates/header', $data);
        echo view('booking/booking_all', $data);
        echo view('templates/footer', $data);
    }

    public function view($id = null) //отображение одной записи
    {

        $model = new BookingModel();
        $data ['booking'] = $model->getBooking($id);
        echo view('templates/header', $data);
        echo view('booking/view', $data);
        echo view('templates/footer', $data);
    }
}
