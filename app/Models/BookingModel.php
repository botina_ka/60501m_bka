<?php namespace App\Models;
use CodeIgniter\Model;
class BookingModel extends Model
{
    protected $table = 'reserv'; //таблица, связанная с моделью
    public function getBooking($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['ID' => $id])->first();
    }
}
